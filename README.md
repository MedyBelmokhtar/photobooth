# PhotoBooth
Un appareil à selfie automatique pour les soirées.

## fonctionnalités
### Minimal
* flash
* envoi vers Google photo ou communauté Google+
* identification des personnes sur la photo (Google cloud vision API)

### FTW
* un écran pour voir la photo
* effets "snapshat" sur les photos
* un écran tactile pour choisir les effets ?
* fond vert ?
* [une imprimante de ticket de caisse](https://www.youtube.com/watch?v=97Fl9DeqkXg) ?

## technique
* un raspberry pi
* webcam ou Reflex usb ?
* structure en bois ou imprimé en 3D?
* sur un trépied
* un gros bouton pour prendre la photo
* un 7 segments pour le décompte
* un module 4G ?

### resources
* [start/stop button](https://howchoo.com/g/mwnlytk3zmm/how-to-add-a-power-button-to-your-raspberry-pi)
* [Use the new Pi 3 to make a touchscreen photo booth that instantly uploads to Google Photos!](http://makezine.com/projects/raspberry-pi-photo-booth/)
* [Mimic Snapchat Filters Programmatically](https://dzone.com/articles/mimic-snapchat-filters-programmatically-1)
* https://petapixel.com/2015/06/06/diy-a-raspberry-pi-photo-booth-you-use-with-your-own-smartphone/
* http://www.diyphotography.net/building-a-vintage-photo-booth-wifi/
* https://www.instructables.com/id/Raspberry-Pi-Photobooth-1/
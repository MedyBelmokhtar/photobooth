# Raspberry pi installation guide

## clone this repo
```
sudo apt-get install -y git
cd ~
git clone https://bitbucket.org/MedyBelmokhtar/photobooth.git
```

## power button
edit ```/etc/rc.local``` to add power script.
```
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi

# add this line
sudo python /home/pi/photobooth/scripts/power.py &

exit 0
```

## photobooth

```bash
sudo apt-get update
// TODO picamera
```

